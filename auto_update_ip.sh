DOSSIER_INSTALL="/opt/e-comBox"
source $DOSSIER_INSTALL/param.conf
source $DOSSIER_INSTALL/fonctions.sh

echo "ADRESSE_IP=$ADRESSE_IP"
NOUVELLE_ADRESSE_IP=$(ip addr show eno1 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1);
echo "NOUVELLE_ADRESSE_IP=$NOUVELLE_ADRESSE_IP"

if [[ $ADRESSE_IP != $NOUVELLE_ADRESSE_IP ]];
then 
	# Modification des parametres dans param.conf
	echo "il faut y aller";
	echo "Re-configuration d'e-comBox le `date`" >> /var/log/ecombox.log;
	echo "" >> /var/log/ecombox.log;
	echo "" >> /var/log/ecombox-error.log;
	echo "Re-configuration d'e-comBox le `date`" >> /var/log/ecombox-error.log;
	echo "" >> /var/log/ecombox-error.log;
	sed -e "s/ADRESSE_IP=$ADRESSE_IP/ADRESSE_IP=$NOUVELLE_ADRESSE_IP/g" $DOSSIER_INSTALL/param.conf > $DOSSIER_INSTALL/param_2.conf;
	mv -f $DOSSIER_INSTALL/param.conf $DOSSIER_INSTALL/param.conf.old;
	mv $DOSSIER_INSTALL/param_2.conf $DOSSIER_INSTALL/param.conf;
	source $DOSSIER_INSTALL/param.conf;
	echo "L'adresse IP ou le nom de domaine utilisé est $ADRESSE_IP" >> /var/log/ecombox.log;
	echo "" >> /var/log/ecombox.log;
	
	# Reverse Proxy Nginx
	# Configuration de l'adresse IP
	echo "Mise-à-jour du reverse proxy Nginx"
	cd $DOSSIER_INSTALL/e-comBox_reverseproxy;
	docker-compose down;
	echo -e "URL_UTILE=$ADRESSE_IP" > $DOSSIER_INSTALL/e-comBox_reverseproxy/.env;
	echo -e "NGINX_PORT=$PORT_RP" >> $DOSSIER_INSTALL/e-comBox_reverseproxy/.env
	# Lancement du reverse proxy
	cd $DOSSIER_INSTALL/e-comBox_reverseproxy/
	docker-compose up -d 2>> /var/log/ecombox-error.log
	if [ $? <> 0 ]; then
  	echo -e "Le reverse proxy a été lancé." >> /var/log/ecombox.log
  	echo "" >> /var/log/ecombox.log
  	else
    	echo -e "Le reverse proxy n'a pas pu être lancé." >> /var/log/ecombox-error.log
    	echo "" >> /var/log/ecombox.log
	fi

	# Portainer
	echo "Mise-à-jour du portainer"
	# Configuration de l'adresse IP dans Portainer
	cd $DOSSIER_INSTALL/e-comBox_portainer
	docker-compose down
	# Récupération de l'ancienne IP et de l'ancien port avant changement
	ANCIENNE_IP=`cat $DOSSIER_INSTALL/e-comBox_portainer/.env | grep "URL" | cut -d"=" -f2`
	ANCIEN_PORT=`cat $DOSSIER_INSTALL/e-comBox_portainer/.env | grep "PORT" | cut -d"=" -f2`
	echo -e "URL_UTILE=$ADRESSE_IP" > $DOSSIER_INSTALL/e-comBox_portainer/.env
	echo -e "PORT=$PORT_PORTAINER" >> $DOSSIER_INSTALL/e-comBox_portainer/.env
	# Lancement de Portainer
	cd $DOSSIER_INSTALL/e-comBox_portainer/
	docker-compose up -d
	if [ $? <> 0 ]; then
  	echo -e "Portainer a été lancé." >> /var/log/ecombox.log
  	echo "" >> /var/log/ecombox.log
  	else
    	echo -e "Portainer n'a pas pu être lancé." >> /var/log/ecombox-error.log
    	echo "" >> /var/log/ecombox.log
	fi
	
	# Redémarrage de l'application
	echo "Redémarrage de l'application"
	# Arrêt
	echo "Arrêt"
	if docker ps -a | grep e-combox; then
		docker rm -f e-combox
	fi
	# Lancement de e-comBox
	echo "Lancement"
	docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $PORT_ECB:80 --network bridge_e-combox reseaucerta/e-combox:$VERSION_APPLI 2>> /var/log/ecombox-error.log
	if [ $? <> 0 ]; then
  		echo -e "e-comBox a été lancée." >> /var/log/ecombox.log
  		echo "" >> /var/log/ecombox.log
  	else  
    		echo -e "e-comBox n'a pas pu être lancée." >> /var/log/ecombox-error.log
    	echo "" >> /var/log/ecombox-error.log
	fi
	# Configuration de l'API
	echo "Configuration de l'API" 
	if [ "$ANCIENNE_IP:$ANCIEN_PORT" != "$ADRESSE_IP:$PORT_PORTAINER" ]; then
   	echo -e "L'URL $ANCIENNE_URL est remplacée par $ADRESSE_IP:$PORT_PORTAINER." >> /var/log/ecombox.log
   	for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js /var/lib/docker/volumes/ecombox_data/_data/*.js.map
   	do
     		sed -i -e "s/$ANCIENNE_IP:$ANCIEN_PORT/$ADRESSE_IP:$PORT_PORTAINER/g" $fichier
   	done
   	else
     	echo -e "Aucun changement à opérer au niveau de $ADRESSE_IP:$PORT_PORTAINER." >> /var/log/ecombox.log
	fi

	# Arrêt des stacks
	echo "arrêt des stacks"
	if [ ! -e $DOSSIER_INSTALL/version2 ]; then 
   STOP_STACKS
	fi
	echo "envoi des courriels"
	python3 /home/infolittre/script/courrieleur.py
fi
exit 0
